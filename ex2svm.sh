#!/bin/sh

exec scala "$0" "$@"
!#
object Conv {
 def main(args:Array[String]) {
   println("converting: " + args(0))

   val lines = io.Source.fromFile(args(0)).getLines.toList
   val converted = lines
     .map(s => s.split(","))
     .map(xs => s"${xs.last},${xs.init.mkString(" ")}")
     .mkString("\n")

  scala.tools.nsc.io.File(args(0) + ".svm").writeAll(converted)

 }
}

Conv.main(args)
