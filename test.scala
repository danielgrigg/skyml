import org.apache.spark.mllib.regression.LinearRegressionWithSGD
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.regression.LinearRegressionModel

type DataSet = Vector[Vector[Double]] 

def loadText(path:String):DataSet = {
  val lines = scala.io.Source.fromFile(path).getLines.toVector
  lines map (s => s.split(',').map(_.toDouble).toVector)
}

def labelledDataSet(data:DataSet) = {
  sc.parallelize(data)
  .map (row => { LabeledPoint(row(0), Vectors.dense(row.tail.toArray))})
  .cache()
}

def valuesAndPreds(model: LinearRegressionModel, 
                   data:RDD[LabeledPoint]) = data.map { point =>
  val prediction = model.predict(point.features)
  (point, prediction)
}

def pretty(labelsPredictions:RDD[(LabeledPoint, Double)]) = {
  labelsPredictions.take(10).foreach({case (vf, p) => 
  println(s"Features: ${vf.features}, Actual: ${vf.label}, Predicted: ${p}")})
}

def evaluate(data:RDD[LabeledPoint]) = {

  val numIterations = 100

  val model = LinearRegressionWithSGD.train(data, numIterations)

  val dataValsPreds = valuesAndPreds(model, data)
  pretty(dataValsPreds)
}


def demoDivergence() = {
  val dataSet = labelledDataSet(loadText("data/ex1data2.txt"))
  evaluate(dataSet)
}
