import org.apache.spark.mllib.regression.LinearRegressionWithSGD
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.regression.LinearRegressionModel
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.feature.StandardScaler
import org.apache.spark.mllib.feature.StandardScalerModel
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.optimization.{SimpleUpdater, SquaredL2Updater, L1Updater}

type DataSet = Vector[Vector[Double]]

// some utility functions
def loadText(path:String):DataSet = {
  val lines = scala.io.Source.fromFile(path).getLines.toVector
  lines map (s => s.split(',').map(_.toDouble).toVector)
}

def column(rows:Vector[Vector[Double]], c:Int) = rows map (r => r(c))


def saveText(data:DataSet, path:String):Unit = {
  val csv = data.map(row => row.mkString(",")).mkString("\n")
  scala.tools.nsc.io.File(path).writeAll(csv)
}

def saveColumns(path:String, xs:Vector[Double], ys:Vector[Double]) = {
  val ps = xs zip ys
  val dataSet = ps.map { case (x:Double,y:Double) => Vector(y, x) }
  saveText(dataSet, path)
}


// Feature scaling - we need to keep the StandardScaler around
// to scale new samples for prediction
def scaler(data:RDD[LabeledPoint]) = {
  new StandardScaler(
    withMean = true, 
    withStd = true).fit(data.map(x => x.features))
}

// Scale a whole data set
def scaleData(s:StandardScalerModel, data:RDD[LabeledPoint]) = {
  val scaledData = data.map(x => 
      LabeledPoint(x.label, s.transform(Vectors.dense(x.features.toArray))))
  scaledData.cache()
}

// Load a data set into a Spark RDD
def labelledDataSet(data:DataSet) = {
  sc.parallelize(data)
  .map (row => { LabeledPoint(row(0), Vectors.dense(row.tail.toArray))})
  .cache()
}

// Apply the model to a labelled RDD
def valuesAndPreds(model: LinearRegressionModel, 
                   data:RDD[LabeledPoint]) = data.map { point =>
  val prediction = model.predict(point.features)
  (point, prediction)
}

// Print the actual vs predictions
def pretty(labelsPredictions:RDD[(LabeledPoint, Double)]) = {
  labelsPredictions.take(10).foreach({case (vf, p) => 
  println(s"Features: ${vf.features}, Actual: ${vf.label}, Predicted: ${p}")})
}

// Create a model for a data set and evaluate it.
def evaluate(data:RDD[LabeledPoint], step:Double,  numIterations:Int) = {

  // The Updater classes mostly just perform regularisation and 
  // modify the step size dynamically.
  // At time of writing there's three Updaters available, see
  // https://spark.apache.org/docs/1.4.0/api/java/org/apache/spark/mllib/optimization/Updater.html
  val updater = new SquaredL2Updater()

  val regParam = 0.01
  val algorithm = new LinearRegressionWithSGD()
  algorithm.optimizer.
    setNumIterations(numIterations).
    setStepSize(step).
    setUpdater(updater).
    setRegParam(regParam)

  algorithm.setIntercept(true)

  val model = algorithm.run(data)
  println(s">>>> Model intercept: ${model.intercept}, weights: ${model.weights}")

  val dataValsPreds = valuesAndPreds(model, data)
  pretty(dataValsPreds)
  model
}

// mean-square-error - the cost function we're trying to minimise.
def MSE(valuesPreds:RDD[(LabeledPoint, Double)]) = 
  valuesPreds.map{case(l, p) => math.pow((l.label - p), 2)}.mean()


def demo(step:Double, numIterations:Int) = {
  val rawAll = loadText("data/ex1data2.txt")
  val dataSetAll = labelledDataSet(rawAll)
  val scalerAll = scaler(dataSetAll)
  val scaledAll = scaleData(scalerAll, dataSetAll)

  evaluate(scaledAll, step, numIterations)
}

def demoCompareNormal(step:Double, numIterations:Int) = {
  val rawAll = loadText("data/ex1data2.txt")
  val dataSetAll = labelledDataSet(rawAll)
  val scalerAll = scaler(dataSetAll)
  val scaledAll = scaleData(scalerAll, dataSetAll)

  val model = evaluate(scaledAll, step, numIterations)

  val raw3Bed = loadText("data/ex1data2_3bed.txt")
  val dataSet3Bed = labelledDataSet(raw3Bed)
  val scaler3Bed = scaler(dataSet3Bed)
  val scaled3Bed = scaleData(scaler3Bed, dataSet3Bed)

  val as = column(raw3Bed, 1)
  val ys = valuesAndPreds(model, scaled3Bed).map(x => x._2).collect.toVector
  
  saveColumns("/tmp/predicted.txt", as, ys)
}
