#!/bin/sh

curl -L -o /tmp/spark.tgz http://mirror.ventraip.net.au/apache/spark/spark-1.4.0/spark-1.4.0-bin-hadoop2.6.tgz
[ -d spark ] || mkdir spark
tar -x -C spark --strip-components 1 -z -f /tmp/spark.tgz
cp spark/conf/log4j.properties.template spark/conf/log4j.properties
perl -pi.bak -e 's/log4j.rootCategory=INFO/log4j.rootCategory=WARN/' spark/conf/log4j.properties
