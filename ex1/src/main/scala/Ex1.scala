import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

object Ex1App {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("ex1 Application")
    val sc = new SparkContext(conf)
    println("Starting Ex1App...")
  }
}
