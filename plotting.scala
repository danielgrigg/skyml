import com.quantifind.charts.Highcharts._

type DataSet = Vector[Vector[Double]]

def loadText(path:String):DataSet = {
  val lines = scala.io.Source.fromFile(path).getLines.toVector
  lines map (s => s.split(',').map(_.toDouble).toVector)
}

def column(rows:Vector[Vector[Double]], c:Int) = rows map (r => r(c))

val raw = loadText("../data/ex1data2_3bed.txt")
val xs = column(raw, 1)
val ys = column(raw, 0)
val predicted = loadText("/tmp/predicted.txt")
val ps = column(predicted, 0)

deleteAll
regression(xs, ys)
hold
line(xs, ps)
