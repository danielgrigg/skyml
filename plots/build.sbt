name := "plots"

organization := "com.example"

version := "0.0.1"

scalaVersion := "2.11.5"

libraryDependencies ++= Seq(
  "com.quantifind" %% "wisp" % "0.0.4"
//  "com.github.wookietreiber" %% "scala-chart" % "latest.integration"
//  "org.sameersingh.scalaplot" % "scalaplot" % "0.0.4"
)

initialCommands := "import com.example.plots._"

