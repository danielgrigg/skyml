
def simpleDataSet(xs:Vector[Double], ys:Vector[Double]) = {
  (ys zip xs) map { case(y,x) => Vector(y, x) }
}

def dummy1(path:String) = { 
  val xs = (1.0 to (10.0, 0.1)).toVector
  val rnd = new Random(0)
  val ys = xs map (x => 6.0 + x + 0.2 * rnd.nextGaussian)
  val data = simpleDataSet(xs, ys)
  saveText(data, path)
}

